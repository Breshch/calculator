﻿
namespace Calculator
{
    partial class Calculator
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Calculator));
            this.textBox_Display = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.groupBox_Numbers = new System.Windows.Forms.GroupBox();
            this.button29 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button28 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.groupBox_Science = new System.Windows.Forms.GroupBox();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.groupBox_Symbols = new System.Windows.Forms.GroupBox();
            this.button12 = new System.Windows.Forms.Button();
            this.richTextBox_Logs = new System.Windows.Forms.RichTextBox();
            this.richTextBox_Log_Full = new System.Windows.Forms.RichTextBox();
            this.checkBox_Science = new System.Windows.Forms.CheckBox();
            this.groupBox_Numbers.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox_Science.SuspendLayout();
            this.groupBox_Symbols.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox_Display
            // 
            this.textBox_Display.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_Display.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.textBox_Display.Location = new System.Drawing.Point(12, 97);
            this.textBox_Display.Name = "textBox_Display";
            this.textBox_Display.Size = new System.Drawing.Size(273, 26);
            this.textBox_Display.TabIndex = 0;
            this.textBox_Display.Text = " ";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button1.Location = new System.Drawing.Point(13, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(54, 44);
            this.button1.TabIndex = 1;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button2.Location = new System.Drawing.Point(73, 23);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(54, 44);
            this.button2.TabIndex = 2;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button3.Location = new System.Drawing.Point(133, 23);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(54, 44);
            this.button3.TabIndex = 3;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button4.Location = new System.Drawing.Point(13, 73);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(54, 44);
            this.button4.TabIndex = 4;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button5.Location = new System.Drawing.Point(73, 73);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(54, 44);
            this.button5.TabIndex = 5;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button6.Location = new System.Drawing.Point(133, 73);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(54, 44);
            this.button6.TabIndex = 6;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button_Click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button7.Location = new System.Drawing.Point(13, 123);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(54, 44);
            this.button7.TabIndex = 7;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button_Click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button8.Location = new System.Drawing.Point(73, 123);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(54, 44);
            this.button8.TabIndex = 8;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button_Click);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button9.Location = new System.Drawing.Point(133, 123);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(54, 44);
            this.button9.TabIndex = 9;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button_Click);
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button10.Location = new System.Drawing.Point(13, 173);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(54, 44);
            this.button10.TabIndex = 10;
            this.button10.Text = "0";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button_Click);
            // 
            // groupBox_Numbers
            // 
            this.groupBox_Numbers.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox_Numbers.Controls.Add(this.button29);
            this.groupBox_Numbers.Controls.Add(this.button11);
            this.groupBox_Numbers.Controls.Add(this.button10);
            this.groupBox_Numbers.Controls.Add(this.button9);
            this.groupBox_Numbers.Controls.Add(this.button8);
            this.groupBox_Numbers.Controls.Add(this.button7);
            this.groupBox_Numbers.Controls.Add(this.button6);
            this.groupBox_Numbers.Controls.Add(this.button5);
            this.groupBox_Numbers.Controls.Add(this.button4);
            this.groupBox_Numbers.Controls.Add(this.button3);
            this.groupBox_Numbers.Controls.Add(this.button2);
            this.groupBox_Numbers.Controls.Add(this.button1);
            this.groupBox_Numbers.Location = new System.Drawing.Point(82, 127);
            this.groupBox_Numbers.Margin = new System.Windows.Forms.Padding(10);
            this.groupBox_Numbers.Name = "groupBox_Numbers";
            this.groupBox_Numbers.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox_Numbers.Size = new System.Drawing.Size(202, 230);
            this.groupBox_Numbers.TabIndex = 19;
            this.groupBox_Numbers.TabStop = false;
            // 
            // button29
            // 
            this.button29.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button29.Location = new System.Drawing.Point(73, 173);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(54, 44);
            this.button29.TabIndex = 10;
            this.button29.Text = "00";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button_Click);
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button11.Location = new System.Drawing.Point(133, 173);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(54, 44);
            this.button11.TabIndex = 12;
            this.button11.Text = ",";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button28);
            this.groupBox1.Controls.Add(this.button18);
            this.groupBox1.Controls.Add(this.button17);
            this.groupBox1.Location = new System.Drawing.Point(11, 127);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(68, 230);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            // 
            // button28
            // 
            this.button28.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button28.Location = new System.Drawing.Point(8, 123);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(54, 44);
            this.button28.TabIndex = 25;
            this.button28.Text = "±";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button_Click);
            // 
            // button18
            // 
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button18.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button18.Location = new System.Drawing.Point(8, 23);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(54, 44);
            this.button18.TabIndex = 19;
            this.button18.Text = "CLR";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button_Click);
            // 
            // button17
            // 
            this.button17.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button17.Location = new System.Drawing.Point(8, 73);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(54, 44);
            this.button17.TabIndex = 18;
            this.button17.Text = "<-";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button_Click);
            // 
            // button19
            // 
            this.button19.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button19.Location = new System.Drawing.Point(65, 72);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(54, 44);
            this.button19.TabIndex = 2;
            this.button19.Text = "1/X";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button_Click);
            // 
            // button20
            // 
            this.button20.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button20.Location = new System.Drawing.Point(6, 73);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(54, 44);
            this.button20.TabIndex = 3;
            this.button20.Text = "Sin";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button_Click);
            // 
            // button21
            // 
            this.button21.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button21.Location = new System.Drawing.Point(6, 173);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(114, 44);
            this.button21.TabIndex = 4;
            this.button21.Text = "¶";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button_Click);
            // 
            // button23
            // 
            this.button23.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button23.Location = new System.Drawing.Point(6, 22);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(54, 44);
            this.button23.TabIndex = 6;
            this.button23.Text = "Double";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button_Click);
            // 
            // button24
            // 
            this.button24.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button24.Location = new System.Drawing.Point(66, 73);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(54, 44);
            this.button24.TabIndex = 7;
            this.button24.Text = "Cos";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button_Click);
            // 
            // button25
            // 
            this.button25.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button25.Location = new System.Drawing.Point(6, 123);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(54, 44);
            this.button25.TabIndex = 8;
            this.button25.Text = "Tg";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button_Click);
            // 
            // button26
            // 
            this.button26.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button26.Location = new System.Drawing.Point(66, 23);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(54, 44);
            this.button26.TabIndex = 9;
            this.button26.Text = "Cube";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button_Click);
            // 
            // groupBox_Science
            // 
            this.groupBox_Science.Controls.Add(this.button26);
            this.groupBox_Science.Controls.Add(this.button20);
            this.groupBox_Science.Controls.Add(this.button21);
            this.groupBox_Science.Controls.Add(this.button25);
            this.groupBox_Science.Controls.Add(this.button24);
            this.groupBox_Science.Controls.Add(this.button23);
            this.groupBox_Science.Location = new System.Drawing.Point(427, 127);
            this.groupBox_Science.Name = "groupBox_Science";
            this.groupBox_Science.Size = new System.Drawing.Size(127, 229);
            this.groupBox_Science.TabIndex = 24;
            this.groupBox_Science.TabStop = false;
            this.groupBox_Science.Visible = false;
            // 
            // button13
            // 
            this.button13.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button13.Location = new System.Drawing.Point(6, 72);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(54, 44);
            this.button13.TabIndex = 13;
            this.button13.Text = "*";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button_Click);
            // 
            // button14
            // 
            this.button14.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button14.Location = new System.Drawing.Point(5, 122);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(54, 44);
            this.button14.TabIndex = 14;
            this.button14.Text = "-";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button_Click);
            // 
            // button27
            // 
            this.button27.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button27.Location = new System.Drawing.Point(65, 22);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(54, 44);
            this.button27.TabIndex = 23;
            this.button27.Text = "%";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button_Click);
            // 
            // button15
            // 
            this.button15.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button15.Location = new System.Drawing.Point(5, 172);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(54, 44);
            this.button15.TabIndex = 24;
            this.button15.Text = "+";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button_Click);
            // 
            // button16
            // 
            this.button16.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button16.Location = new System.Drawing.Point(6, 22);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(54, 44);
            this.button16.TabIndex = 25;
            this.button16.Text = "/";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button_Click);
            // 
            // groupBox_Symbols
            // 
            this.groupBox_Symbols.Controls.Add(this.button19);
            this.groupBox_Symbols.Controls.Add(this.button12);
            this.groupBox_Symbols.Controls.Add(this.button16);
            this.groupBox_Symbols.Controls.Add(this.button15);
            this.groupBox_Symbols.Controls.Add(this.button27);
            this.groupBox_Symbols.Controls.Add(this.button14);
            this.groupBox_Symbols.Controls.Add(this.button13);
            this.groupBox_Symbols.Location = new System.Drawing.Point(291, 128);
            this.groupBox_Symbols.Name = "groupBox_Symbols";
            this.groupBox_Symbols.Size = new System.Drawing.Size(125, 229);
            this.groupBox_Symbols.TabIndex = 20;
            this.groupBox_Symbols.TabStop = false;
            // 
            // button12
            // 
            this.button12.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button12.Location = new System.Drawing.Point(65, 126);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(54, 90);
            this.button12.TabIndex = 26;
            this.button12.Text = "=";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button_Click);
            // 
            // richTextBox_Logs
            // 
            this.richTextBox_Logs.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.richTextBox_Logs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox_Logs.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.richTextBox_Logs.Location = new System.Drawing.Point(290, 37);
            this.richTextBox_Logs.Name = "richTextBox_Logs";
            this.richTextBox_Logs.ReadOnly = true;
            this.richTextBox_Logs.Size = new System.Drawing.Size(126, 83);
            this.richTextBox_Logs.TabIndex = 25;
            this.richTextBox_Logs.Text = "";
            this.richTextBox_Logs.TextChanged += new System.EventHandler(this.richTextBox_TextChanged);
            // 
            // richTextBox_Log_Full
            // 
            this.richTextBox_Log_Full.BackColor = System.Drawing.Color.AntiqueWhite;
            this.richTextBox_Log_Full.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox_Log_Full.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.richTextBox_Log_Full.Location = new System.Drawing.Point(12, 38);
            this.richTextBox_Log_Full.Name = "richTextBox_Log_Full";
            this.richTextBox_Log_Full.Size = new System.Drawing.Size(273, 53);
            this.richTextBox_Log_Full.TabIndex = 26;
            this.richTextBox_Log_Full.Text = "";
            this.richTextBox_Log_Full.TextChanged += new System.EventHandler(this.richTextBox_TextChanged);
            // 
            // checkBox_Science
            // 
            this.checkBox_Science.AutoSize = true;
            this.checkBox_Science.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.checkBox_Science.Location = new System.Drawing.Point(11, 12);
            this.checkBox_Science.Name = "checkBox_Science";
            this.checkBox_Science.Size = new System.Drawing.Size(66, 19);
            this.checkBox_Science.TabIndex = 27;
            this.checkBox_Science.Text = "Science";
            this.checkBox_Science.UseVisualStyleBackColor = true;
            this.checkBox_Science.CheckedChanged += new System.EventHandler(this.CheckBox_Science);
            // 
            // Calculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(562, 363);
            this.Controls.Add(this.checkBox_Science);
            this.Controls.Add(this.richTextBox_Log_Full);
            this.Controls.Add(this.richTextBox_Logs);
            this.Controls.Add(this.groupBox_Science);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox_Symbols);
            this.Controls.Add(this.textBox_Display);
            this.Controls.Add(this.groupBox_Numbers);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Calculator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculator";
            this.groupBox_Numbers.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox_Science.ResumeLayout(false);
            this.groupBox_Symbols.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_Display;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.GroupBox groupBox_Numbers;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.GroupBox groupBox_Science;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.GroupBox groupBox_Symbols;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.RichTextBox richTextBox_Logs;
        private System.Windows.Forms.RichTextBox richTextBox_Log_Full;
        private System.Windows.Forms.CheckBox checkBox_;
        private System.Windows.Forms.CheckBox checkBox_Science;
    }
}

