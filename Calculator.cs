﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Calculator : Form
    {
        public Calculator()
        {
            InitializeComponent();
        }

        public double a = 0;
        public double b = 0;
        public double x = 0;
        public string tmp_a;
        public string button_Text;
        public bool clr = false;
        public string equal;
        public string symb;

        public void button_Click(object sender, EventArgs e)
        {
            var button = (Button)sender;
            button_Text = button.Text;
            if (IsDigital(button_Text))
            {
                textBox_Display.Text += button.Text;
                tmp_a += button_Text;
                richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                richTextBox_Log_Full.Text += button.Text;
            }
            else if (button_Text == "CLR")
            {
                textBox_Display.Clear();
                richTextBox_Log_Full.Clear();
                richTextBox_Logs.Text += Environment.NewLine + button_Text;
                clr = true;
                Clear(clr);
                richTextBox_Logs.Text += Environment.NewLine + button_Text;
            }
            else
                switch (button_Text)
                {
                    case "±":
                        {
                            if (double.Parse(tmp_a) > 0 && tmp_a.StartsWith("-"))
                            {
                                textBox_Display.Text = button_Text.Remove(0);
                                richTextBox_Logs.Text += button_Text;
                                richTextBox_Log_Full.Text += button_Text;
                            }
                            textBox_Display.Text = tmp_a.Insert(0, "-");
                            richTextBox_Logs.Text += Environment.NewLine + button_Text;
                            richTextBox_Log_Full.Text += button_Text;
                            break;
                        }
                    case "<-":
                        if (textBox_Display.Text == "")
                        {
                            break;
                        }
                        textBox_Display.Text = textBox_Display.Text.Substring(0, textBox_Display.Text.Length - 1);
                        tmp_a = textBox_Display.Text;
                        richTextBox_Logs.Text += Environment.NewLine + button_Text;
                        richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                        richTextBox_Log_Full.Text += button_Text;
                        richTextBox_Log_Full.Text += textBox_Display.Text;
                        break;
                    case "CLR":
                        clr = true;
                        Clear(clr);
                        richTextBox_Logs.Text += Environment.NewLine + button_Text;
                        break;
                    case "SQRT":
                        if (IsEmpty())
                            break;
                        x = Math.Sqrt(Double.Parse(textBox_Display.Text));
                        textBox_Display.Text = (x.ToString());
                        tmp_a = x.ToString();
                        richTextBox_Logs.Text += Environment.NewLine + button_Text;
                        richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                        richTextBox_Log_Full.Text += button_Text;
                        richTextBox_Log_Full.Text += equal + " = " + textBox_Display.Text + Environment.NewLine;
                        richTextBox_Log_Full.Text += Environment.NewLine;
                        break;
                    case "Double":
                        if (IsEmpty())
                            break;
                        x = Math.Pow(Double.Parse(textBox_Display.Text), 2);
                        textBox_Display.Text = (x.ToString());
                        tmp_a = x.ToString();
                        richTextBox_Logs.Text += Environment.NewLine + button_Text;
                        richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                        richTextBox_Log_Full.Text += button_Text;
                        richTextBox_Log_Full.Text += equal + " = " + textBox_Display.Text + Environment.NewLine;
                        richTextBox_Log_Full.Text += Environment.NewLine;
                        break;
                    case "Cube":
                        if (IsEmpty())
                            break;
                        x = Math.Pow(Double.Parse(textBox_Display.Text), 3);
                        textBox_Display.Text = (x.ToString());
                        tmp_a = x.ToString();
                        richTextBox_Logs.Text += Environment.NewLine + button_Text;
                        richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                        richTextBox_Log_Full.Text += button_Text;
                        richTextBox_Log_Full.Text += equal + " = " + textBox_Display.Text + Environment.NewLine;
                        richTextBox_Log_Full.Text += Environment.NewLine;
                        break;
                    case "Sin":
                        if (IsEmpty())
                            break;
                        x = Math.Sin(Double.Parse(textBox_Display.Text) * ((Math.PI)) / 180);
                        textBox_Display.Text = (Math.Round(x, 2).ToString());
                        tmp_a = x.ToString();
                        richTextBox_Logs.Text += Environment.NewLine + button_Text;
                        richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                        richTextBox_Log_Full.Text += button_Text;
                        richTextBox_Log_Full.Text += equal + " = " + textBox_Display.Text + Environment.NewLine;
                        richTextBox_Log_Full.Text += Environment.NewLine;
                        break;
                    case "Cos":
                        if (IsEmpty())
                            break;
                        x = Math.Sin((90 - Double.Parse(textBox_Display.Text)) * ((Math.PI)) / 180);
                        textBox_Display.Text = (Math.Round(x, 2).ToString());
                        tmp_a = x.ToString();
                        richTextBox_Logs.Text += Environment.NewLine + button_Text;
                        richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                        richTextBox_Log_Full.Text += button_Text;
                        richTextBox_Log_Full.Text += equal + " = " + textBox_Display.Text + Environment.NewLine;
                        richTextBox_Log_Full.Text += Environment.NewLine;
                        break;
                    case "Tg":
                        if (IsEmpty())
                            break;
                        x = Math.Sin((Double.Parse(textBox_Display.Text) * ((Math.PI)) / 180)) /
                            (Math.Sin((90 - Double.Parse(tmp_a)) * ((Math.PI)) / 180));
                        textBox_Display.Text = (Math.Round(x, 15).ToString());
                        tmp_a = x.ToString();
                        richTextBox_Logs.Text += Environment.NewLine + button_Text;
                        richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                        richTextBox_Log_Full.Text += button_Text;
                        richTextBox_Log_Full.Text += equal + " = " + textBox_Display.Text + Environment.NewLine;
                        richTextBox_Log_Full.Text += Environment.NewLine;
                        break;
                    case "1/X":
                        if (Double.Parse(tmp_a) == 0 && !IsEmpty())
                        {
                            Clear(clr = true);
                        }
                        string tmp = textBox_Display.Text;
                        x = 1 / Double.Parse(textBox_Display.Text);
                        textBox_Display.Text = (Math.Round(x, 15).ToString());
                        tmp_a = x.ToString();
                        button_Text = button_Text.Remove(2);
                        button_Text = button_Text + tmp;
                        richTextBox_Logs.Text += Environment.NewLine + button_Text;
                        richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                        richTextBox_Log_Full.Text += Environment.NewLine;
                        richTextBox_Log_Full.Text += button_Text;
                        richTextBox_Log_Full.Text += equal + " = " + textBox_Display.Text + Environment.NewLine;
                        richTextBox_Log_Full.Text += Environment.NewLine;
                        break;
                    case "+":
                        a = Double.Parse(textBox_Display.Text);
                        button_Text = "+";
                        richTextBox_Logs.Text += Environment.NewLine + button_Text;
                        richTextBox_Log_Full.Text += button_Text;
                        symb = button_Text;
                        Clear(clr);
                        break;
                    case "-":
                        a = Double.Parse(textBox_Display.Text);
                        button_Text = "-";
                        richTextBox_Logs.Text += Environment.NewLine + button_Text;
                        richTextBox_Log_Full.Text += button_Text;
                        Clear(clr);
                        symb = button_Text;
                        break;
                    case "*":
                        a = Double.Parse(textBox_Display.Text);
                        button_Text = "*";
                        richTextBox_Logs.Text += Environment.NewLine + button_Text;
                        richTextBox_Log_Full.Text += button_Text;
                        symb = button_Text;
                        Clear(clr);
                        break;
                    case "/":
                        a = Double.Parse(textBox_Display.Text);
                        button_Text = "/";
                        richTextBox_Logs.Text += Environment.NewLine + button_Text;
                        richTextBox_Log_Full.Text += button_Text;
                        symb = button_Text;
                        Clear(clr);
                        break;
                    case ",":
                        if (textBox_Display.Text.Contains(","))
                        {
                            return;
                        }
                        textBox_Display.Text += ",";
                        richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                        richTextBox_Log_Full.Text += textBox_Display.Text;
                        break;
                    case "¶":
                        if (textBox_Display.Text != "")
                        {
                            a = Double.Parse(textBox_Display.Text);
                            richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                            richTextBox_Log_Full.Text += textBox_Display.Text;
                            richTextBox_Log_Full.Text += Environment.NewLine;
                            Clear(clr);
                        }
                        textBox_Display.Text = Math.Round(Math.PI, 2).ToString();
                        richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                        richTextBox_Log_Full.Text += textBox_Display.Text;
                        break;
                    case "%":
                        b = Double.Parse(textBox_Display.Text);
                        richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                        Clear(clr);
                        switch (symb)
                        {
                            case "+":
                                x = a + (a * b / 100);
                                textBox_Display.Text = (x.ToString());
                                tmp_a = x.ToString();
                                richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                                richTextBox_Log_Full.Text += "% = " + textBox_Display.Text;
                                richTextBox_Log_Full.Text += Environment.NewLine;
                                break;
                            case "-":
                                x = a - (a * b / 100);
                                textBox_Display.Text = (x.ToString());
                                tmp_a = x.ToString();
                                richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                                richTextBox_Log_Full.Text += "% = " + textBox_Display.Text;
                                richTextBox_Log_Full.Text += Environment.NewLine;
                                break;
                            case "*":
                                x = a * (a * b / 100);
                                textBox_Display.Text = (x.ToString());
                                tmp_a = x.ToString();
                                richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                                richTextBox_Log_Full.Text += "% = " + textBox_Display.Text;
                                richTextBox_Log_Full.Text += Environment.NewLine;
                                break;
                            case "/":
                                x = a * (b / 100);
                                textBox_Display.Text = (x.ToString());
                                tmp_a = x.ToString();
                                richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                                richTextBox_Log_Full.Text += "% = " + textBox_Display.Text;
                                richTextBox_Log_Full.Text += Environment.NewLine;

                                break;
                            case " ":
                                break;
                        }
                        break;
                    case "=":
                        if (textBox_Display.Text == "")
                        {
                            break;
                        }

                        b = Double.Parse(textBox_Display.Text);
                        richTextBox_Logs.Text += Environment.NewLine + button_Text;
                        richTextBox_Log_Full.Text += button_Text;
                        Clear(clr);
                        switch (symb)
                        {
                            case "+":
                                x = a + b;
                                equal = x.ToString();
                                textBox_Display.Text = (Math.Round(x, 2).ToString());
                                tmp_a = x.ToString();
                                richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                                richTextBox_Log_Full.Text += textBox_Display.Text + Environment.NewLine;
                                break;
                            case "-":
                                x = a - b;
                                equal = x.ToString();
                                textBox_Display.Text = (Math.Round(x, 2).ToString());
                                tmp_a = x.ToString();
                                richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                                richTextBox_Log_Full.Text += textBox_Display.Text + Environment.NewLine;
                                break;
                            case "*":
                                x = a * b;
                                equal = x.ToString();
                                textBox_Display.Text = (Math.Round(x, 2).ToString());
                                tmp_a = x.ToString();
                                richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                                richTextBox_Log_Full.Text += textBox_Display.Text + Environment.NewLine;
                                break;
                            case "/":
                                x = a / b;
                                equal = x.ToString();
                                textBox_Display.Text = (Math.Round(x, 2).ToString());
                                tmp_a = x.ToString();
                                richTextBox_Logs.Text += Environment.NewLine + textBox_Display.Text;
                                richTextBox_Log_Full.Text += textBox_Display.Text + Environment.NewLine;
                                break;
                            case " ":
                                break;
                        }

                        break;
                    case " ":
                        break;
                }
        }

        

        private bool IsDigital(string text)
        {
            return int.TryParse(text, out _);
        }

        private bool IsEmpty()
        {
            if (textBox_Display.Text == "")
            {

                return true;
            }
            else
            {
                return false;
            }
        }

        public void Clear(bool full)
        {
            if (full)
            {
                a = 0;
                b = 0;
                x = 0;
                tmp_a = "";
                button_Text = " ";
                richTextBox_Log_Full.Clear();
            }

            textBox_Display.Clear();
            clr = false;
        }
        private void richTextBox_TextChanged(object sender, EventArgs e)
        {
            // set the current caret position to the end
            richTextBox_Logs.SelectionStart = richTextBox_Logs.Text.Length;
            richTextBox_Log_Full.SelectionStart = richTextBox_Log_Full.Text.Length;
            // scroll it automatically
            richTextBox_Logs.ScrollToCaret();
            richTextBox_Log_Full.ScrollToCaret();
        }
         private void CheckBox_Science(object sender, EventArgs e)
         {
             groupBox_Science.Visible = checkBox_Science.Checked;
         }
      
    }
}






//private void button1_Click(object sender, EventArgs e)
//{
//    textBox_Display.Text += "1";
//}

//private void button2_Click(object sender, EventArgs e)
//{
//    textBox_Display.Text += "2";
//}

//private void button3_Click(object sender, EventArgs e)
//{
//    textBox_Display.Text += "3";
//}

//private void button4_Click(object sender, EventArgs e)
//{
//    textBox_Display.Text += "4";
//}

//private void button5_Click(object sender, EventArgs e)
//{
//    textBox_Display.Text += "5";
//}

//private void button6_Click(object sender, EventArgs e)
//{
//    textBox_Display.Text += "6";
//}

//private void button7_Click(object sender, EventArgs e)
//{
//    textBox_Display.Text += "7";
//}

//private void button8_Click(object sender, EventArgs e)
//{
//    textBox_Display.Text += "8";
//}

//private void button9_Click(object sender, EventArgs e)
//{
//    textBox_Display.Text += "9";
//}

//private void button10_Click(object sender, EventArgs e)
//{
//    textBox_Display.Text += "0";
//}

//private void button11_Click(object sender, EventArgs e)
//{
//    if (textBox_Display.Text.Contains(","))
//        return;
//    textBox_Display.Text += ",";
//}

//private void button15_Click(object sender, EventArgs e)
//{
//    a = Double.Parse(textBox_Display.Text);
//    button_Text = '+';
//    Clear();
//}

//private void button14_Click(object sender, EventArgs e)
//{
//    a = Double.Parse(textBox_Display.Text);
//    button_Text = '-';
//    Clear();
//}

//private void button13_Click(object sender, EventArgs e)
//{
//    a = Double.Parse(textBox_Display.Text);
//    button_Text = '*';
//    Clear();
//}

//private void button16_Click(object sender, EventArgs e)
//{
//    a = Double.Parse(textBox_Display.Text);
//    button_Text = '/';
//    Clear(); ;
//}

//private void button12_Click(object sender, EventArgs e)
//{
//  b = Double.Parse(textBox_Display.Text);
//  Clear();
//    switch (button_Text)
//  {
//        case '+':
//            x = a + b;
//            textBox_Display.Text = (x.ToString());
//            break;
//        case '-':
//            x = a - b;
//            textBox_Display.Text = (x.ToString());
//            break;
//        case '*':
//            x = a * b;
//            textBox_Display.Text = (x.ToString());
//            break;
//        case '/':
//            x = a / b;
//            textBox_Display.Text = (x.ToString());
//            break;
//        case ' ': 
//            break;
//    }
//}
//private void button17_Click(object sender, EventArgs e)
//{
//    textBox_Display.Text = textBox_Display.Text.Substring(0, textBox_Display.Text.Length - 1);
//}
//private void button18_Click(object sender, EventArgs e)
//{
//    Clear();
//}
